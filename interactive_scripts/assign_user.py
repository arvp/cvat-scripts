#!/usr/bin/env python3
# Assign a user to a job
import os
import pandas as pd
import random
import subprocess
import sys
import tempfile
from pathlib import Path
from termcolor import colored
from tqdm import tqdm


def print_warn(s):
    print(colored(s, "yellow"))


# Setup user validation
user_input = ""

if not ".git" in os.listdir():
    print("Must be in top level git directory to run this script")
    exit(1)

# Get user list
_, users_file = tempfile.mkstemp()
_, jobs_file = tempfile.mkstemp()

print("Downloading cvat tables...")
subprocess.run(
    [
        sys.executable,
        "automated_scripts/get_cvat_tables.py",
        "--quiet",
        "--users",
        users_file,
        "--jobs",
        jobs_file,
    ],
    check=True,
)

df_users = pd.read_csv(users_file)
os.remove(users_file)

df_jobs = pd.read_csv(jobs_file)
os.remove(jobs_file)

# Specify project
options = list(df_jobs["project_name"].unique())

while True:
    print("Choose one project (by number):")
    for i, option in enumerate(options):
        print(f" {i+1}. {option}")

    try:
        user_input = int(input("Choice(s): ").strip())

        if 1 <= user_input <= i + 1:
            break
        else:
            print_warn("Invalid choice")
    except ValueError:
        print_warn("Invalid input")

choice = options[user_input - 1]
df_jobs = df_jobs[df_jobs["project_name"] == choice]


# Build out user stats for selected project
def at_stage(uid, stage):
    uj = df_jobs[df_jobs["assignee_id"] == uid]
    return uj[uj["stage"] == stage]["frame_count"].sum()


df_users["completed_frames"] = df_users["id"].apply(lambda x: at_stage(x, "acceptance"))
df_users["pending_validation_frames"] = df_users["id"].apply(
    lambda x: at_stage(x, "validation")
)
df_users["not_done_frames"] = df_users["id"].apply(lambda x: at_stage(x, "annotation"))

df_users["assigned_frames"] = df_users.apply(
    lambda row: row["completed_frames"]
    + row["pending_validation_frames"]
    + row["not_done_frames"],
    axis=1,
)

# Specify user(s)
options = list(df_users["id"].unique())

while True:
    skip_outer = False

    print("Choose one or more users (by id):")
    print(df_users.to_string(index=False))
    # for i, uid in enumerate(options):
    #    user = df_users[df_users["id"] == uid].iloc[0]

    #    fdone = user["completed_frames"]
    #    fval = user["pending_validation_frames"]
    #    ftot = user["assigned_frames"]

    #    print(
    #        f" {i+1:2d}. {user.username:20} :: complete: {fdone:4} | val: {fval:4} | assigned: {ftot:4}"
    #    )

    try:
        user_input = input("Choice(s): ").strip()

        chosen_users = list()

        for x in user_input.split():
            c = int(x)

            if 1 <= c <= len(df_users):
                chosen_users.append(c)
            else:
                print_warn("Invalid index")
                skip_outer = True
                break

        if skip_outer:
            continue
        else:
            break
    except ValueError:
        print_warn("Invalid input")
        continue


# Specify number of jobs to give each user
while True:
    try:
        nb_jobs = int(input("How many jobs should each user be given? "))
        break
    except ValueError:
        continue

available_jobs = list(df_jobs[df_jobs["assignee_id"].isna()]["id"].unique())
random.shuffle(available_jobs)

if (n := len(chosen_users)) * (y := nb_jobs) < (x := len(available_jobs)):
    print(f"Only {x} unassigned jobs found... insufficent for {n} users @ {y} each")
    print("Jobs will be evenly distributed (some users many get one more)\n")

print("Assigning jobs...")
for x in range(nb_jobs):
    for uid in chosen_users:
        try:
            subprocess.run(
                [
                    sys.executable,
                    "automated_scripts/update_job.py",
                    "--user",
                    str(uid),
                    str(available_jobs.pop()),
                ],
                check=True,
            )
        except IndexError:
            exit(0)
