#!/usr/bin/env python3
# Download images from assigned jobs
import os
import pandas as pd
import subprocess
import sys
import tempfile
from pathlib import Path
from termcolor import colored
from tqdm import tqdm

# Setup path completion in input()
import readline, glob


def complete(text, state):
    return (glob.glob(text + "*") + [None])[state]


readline.set_completer_delims(" \t\n;")
readline.parse_and_bind("tab: complete")
readline.set_completer(complete)


def print_warn(s):
    print(colored(s, "yellow"))


# Setup user validation
user_input = ""

if not ".git" in os.listdir():
    print("Must be in top level git directory to run this script")
    exit(1)

# Get job list
_, jobs_file = tempfile.mkstemp()

print("Downloading jobs table...")
subprocess.run(
    [
        sys.executable,
        "automated_scripts/get_cvat_tables.py",
        "--jobs",
        jobs_file,
    ],
    check=True,
    stdout=subprocess.DEVNULL,
)

df_jobs = pd.read_csv(jobs_file)
os.remove(jobs_file)


# Choice functions
def choose_rows_filter(column):
    """Filter by project name"""
    global df_jobs

    options = list(df_jobs[column].unique())

    while True:
        skip_outer = False

        print("Choose one or more (by number):")
        for i, option in enumerate(options):
            print(f" {i+1}. {option}")

        try:
            user_input = input("Choice(s): ").strip()

            choices = list()

            for x in user_input.split():
                c = int(x)

                if 1 <= c <= i + 1:
                    choices.append(c)
                else:
                    print_warn("Invalid index")
                    skip_outer = True
                    break

            if skip_outer:
                continue
            else:
                break
        except ValueError:
            print_warn("Invalid input")
            continue

    choices = set([options[x - 1] for x in choices])
    df_jobs = df_jobs[df_jobs[column].isin(choices)]


# Prompt user for filters
main_menu = [
    {
        "menu_title": "Specify project (names will be listed)",
        "banner_title": "Project Name",
        "column": "project_name",
    },
    {
        "menu_title": "Specify stage {annotation, validation, acceptance}",
        "banner_title": "Stage",
        "column": "stage",
    },
    {
        "menu_title": "Specify state {new, in progress, completed}",
        "banner_title": "State",
        "column": "state",
    },
    {
        "menu_title": "Specify username (names will be listed)",
        "banner_title": "Username",
        "column": "assignee_username",
    },
]

while True:
    print(
        "\n>>>> "
        + colored("Apply Filters", "green")
        + " >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    )
    print("Choose (by number):")
    for i, row in enumerate(main_menu):
        print(f" {i+1}. {row['menu_title']}")
    print("\n`next` finish applying filters and continue to args")
    print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")

    user_input = input("Choice: ").strip()

    if user_input == "next":
        break

    try:
        user_input = int(user_input)

        if len(main_menu) < user_input or user_input <= 0:
            print_warn("Invalid choice")
            continue

        menu_choice = main_menu.pop(user_input - 1)
    except ValueError:
        continue

    print(f"\n\n==== {menu_choice['banner_title']} =============")
    choose_rows_filter(menu_choice["column"])

_, jobs_file = tempfile.mkstemp()
df_jobs["id"].to_csv(jobs_file, index=False, header=False)

# Fill in download arguments
is_blanks = None

while True:
    user_input = input("Include blank images? (y/n) ").lower().strip()

    if user_input in ["y", "n", "yes", "no"]:
        is_blanks = user_input[0] == "y"
        break
    else:
        print_warn("Invalid choice\n")

output_dir = None

while True:
    user_input = input("Output directory: ")

    output_dir = Path(user_input)

    if output_dir.exists() and len(os.listdir(output_dir)) == 0:
        os.rmdir(output_dir)
        break
    elif not output_dir.exists() and output_dir.parent.is_dir():
        break
    else:
        print_warn("Invalid path\n")

tvt_split = None

while True:
    print('Enter a train-val-test split (must add to 100) or "no" for no split')
    user_input = input("Train-val-test split? (80-10-10/n) ").lower().strip()

    if user_input in ["n", "no"]:
        tvt_split = None
        break
    elif len(user_input.split("-")) != 3:
        print_warn("Invalid format, must be like 80-10-10\n")
        continue

    try:
        tvt_split = [int(x) for x in user_input.split("-")]

        if sum(tvt_split) == 100 and all(x >= 0 for x in tvt_split):
            break

        print_warn("Train-Val-Test splits don't add up to 100\n")
    except ValueError:
        print_warn("Invalid format, must be like 80-10-10\n")

# Run download
download_cmd = [
    sys.executable,
    "automated_scripts/download_jobs.py",
    jobs_file,
    output_dir,
]

if not is_blanks:
    download_cmd.insert(2, "--only-labeled")

if tvt_split is not None:
    download_cmd.insert(2, "-".join([str(t) for t in tvt_split]))
    download_cmd.insert(2, "--tvt-split")

print("Starting download...")
subprocess.run(download_cmd, check=True)
print("Download complete")
