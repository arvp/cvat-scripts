#!/usr/bin/env bash
if [[ $# -ne 0 ]]; then
  cat <<HELP
Download the user and job tables as csvs using get_cvat_tables.py into
  /dev/shm/jobs.csv
  /dev/shm/users.csv
HELP

  exit 1
elif [[ -d .git ]]; then
    python3 ./automated_scripts/get_cvat_tables.py --jobs /dev/shm/jobs.csv --users /dev/shm/users.csv
else
    python3 ../automated_scripts/get_cvat_tables.py --jobs /dev/shm/jobs.csv --users /dev/shm/users.csv
fi
