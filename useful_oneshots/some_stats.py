#!/usr/bin/env python3
import os
import pandas as pd
import subprocess
import sys
import tempfile
from tqdm import tqdm

# Make sure we're in the TLD
if ".git" not in os.listdir():
    os.chdir("..")

assert ".git" in os.listdir(), "Not at top-level git directory"

# Grab jobs table
_, jobs_file = tempfile.mkstemp()
_, users_file = tempfile.mkstemp()

subprocess.run(
    [
        sys.executable,
        "automated_scripts/get_cvat_tables.py",
        "--jobs", jobs_file,
        "--users", users_file,
    ],
    check=True,
)

# total
# total assigned
# total completed

df_jobs = pd.read_csv(jobs_file).set_index("id")
df_users = pd.read_csv(users_file).set_index("id")
os.remove(jobs_file)
os.remove(users_file)

total = df_jobs['frame_count'].sum()
total_assigned = df_users['assigned_frames'].sum()
total_complete = df_users['completed_frames'].sum()
validation = df_jobs[(df_jobs['stage'] == 'validation') & (df_jobs['state'] == 'new')]['frame_count'].sum()
should_be_updated = df_jobs[(df_jobs['stage'] != 'acceptance') & (df_jobs['state'] == 'completed')]['frame_count'].sum()

print(f"Assigned frames: {total_assigned}/{total} = {total_assigned/total*100:0.2f}")
print(f"Completed frames: {total_complete}/{total_assigned} = {total_complete/total_assigned*100:0.2f}")
print(f"Waiting for validation: {validation}")

if should_be_updated > 0:
    print(f"{should_be_updated} frames should be automatically advanced to the next stage")
else:
    print("No frames to automatically advance to the next stage at this time")
