#!/usr/bin/env python3
# Download images from assigned jobs
import argparse
import csv
import json
import os
import random
import requests as rq
import shutil
import tempfile
import uuid
import yaml
import zipfile
from pathlib import Path
from tqdm import tqdm

parser = argparse.ArgumentParser(
    prog="Download Cvat Jobs v1.0.0",
    description="Download content of provided cvat jobs into a directory",
)


def tvt_split_validation(astring):
    if len(astring.split("-")) != 3:
        raise ValueError
    astring = [int(x) for x in astring.split("-")]
    if sum(astring) != 100:
        raise ValueError("Train-Validation-Test split does not sum to 100")
    return astring


parser.add_argument(
    "--tvt-split",
    action="store",
    type=tvt_split_validation,
    help="Test-Val-Train split images. Must add to 100",
)

parser.add_argument(
    "--only-labeled",
    action="store_true",
    default=0,
    help="Remove all images that don't have a label",
)

parser.add_argument(
    "job_ids_file",
    type=str,
    metavar="<job-id-file>",
    help="A file of job ids to download, one per line",
)

parser.add_argument(
    "dir",
    type=str,
    metavar="<output-dir>",
    help="Directory to output aggregated downloads",
)

args = parser.parse_args()

TVT_SPLIT = args.tvt_split

# Load config >>>>
if os.path.exists("config.yml"):
    with open("config.yml", "r") as f:
        config = yaml.safe_load(f)
else:
    with open("../config.yml", "r") as f:
        config = yaml.safe_load(f)

OUTPUT_DIR = args.dir
IS_BLANKS = not args.only_labeled

if os.path.exists(OUTPUT_DIR):
    print(f"Error: Path `{OUTPUT_DIR}` already exists")
    exit(1)


def api_url(path):
    c = config["connection"]
    return f"{c['protocol']}://{c['domain']}:{c['port']}/api/{path}"


login_resp = rq.post(api_url("auth/login"), json=config["authentication"])
cookies = login_resp.cookies
# <<<<<<<<

# Get job ids to download
with open(args.job_ids_file, "r") as f:
    job_ids = set([int(line.strip()) for line in f])

for job_id in tqdm(job_ids):
    job_url = api_url(f"jobs/{job_id}/dataset?format=YOLO 1.1&action=download")

    # Keep requesting while download isn't ready
    while True:
        response = rq.get(job_url, cookies=cookies)

        if response.status_code == 405:
            raise Exception(f"Format is not available for api call: `{get_url}`")
        elif response.status_code in [201, 202]:
            continue
        else:
            break

    # Extract into temporary location
    _, zip_path = tempfile.mkstemp(prefix="cvat__", suffix=".zip")

    with open(zip_path, "bw") as f:
        f.write(response.content)

    unzip_path = tempfile.mkdtemp(prefix="cvat__")

    with zipfile.ZipFile(zip_path, "r") as f:
        f.extractall(unzip_path)

    os.remove(zip_path)

    # Move images to output destination
    if not os.path.exists(OUTPUT_DIR):
        os.makedirs(OUTPUT_DIR)

    with open(f"{unzip_path}/train.txt", "r") as f:
        for line in f:
            p_image = Path(f"{unzip_path}/{line[5:].strip()}")
            p_label = p_image.with_suffix(".txt")

            # Don't move blank images if --only-labeled flag is provided
            if not IS_BLANKS and p_label.stat().st_size == 0:
                continue

            salt = str(uuid.uuid4())

            o_image = Path(f"{OUTPUT_DIR}/{salt}_{p_image.name}")
            o_label = Path(f"{OUTPUT_DIR}/{salt}_{p_label.name}")

            shutil.copyfile(p_image, o_image)
            shutil.copyfile(p_label, o_label)

    shutil.rmtree(unzip_path)

# Do a train-validation-test split in the YOLO 1.1 format
if TVT_SPLIT is not None:
    tvt_splits = [Path(f"{OUTPUT_DIR}/{x}") for x in os.listdir(OUTPUT_DIR)]
    tvt_splits = list(filter(lambda x: x.suffix != ".txt", tvt_splits))

    random.shuffle(tvt_splits)

    s1 = int(len(tvt_splits) * TVT_SPLIT[0] / 100)
    s2 = s1 + int(len(tvt_splits) * TVT_SPLIT[1] / 100)

    tvt_splits = [
        ("train", tvt_splits[:s1]),
        ("valid", tvt_splits[s1:s2]),
        ("test", tvt_splits[s2:]),
    ]

    for set_name, split_set in tvt_splits:
        set_dir = Path(f"{OUTPUT_DIR}/{set_name}")
        os.makedirs(set_dir)

        with open(f"{OUTPUT_DIR}/{set_name}.txt", "w") as f:
            for p in split_set:
                p_new = set_dir / p.name
                o = p.with_suffix(".txt")
                o_new = set_dir / o.name

                shutil.move(p, p_new)
                shutil.move(o, o_new)

                f.write(f"{set_name}/{p.name}\n")
