#!/usr/bin/env python3
# Assigns Cvat jobs to a user
import argparse
import json
import os
import requests as rq
import yaml

parser = argparse.ArgumentParser(
    prog="Assign Cvat job v1.0.0", description="Update a job's attributes"
)

    # Initial stage is annotation
parser.add_argument(
    "--stage",
    action="store",
    help="Set the job's stage. One of {annotation, validation, acceptance}",
)

    # Initial state is new
parser.add_argument(
    "--state",
    action="store",
    help="Set the job's state. One of {new, in progress, completed, rejected}",
)

parser.add_argument(
    "--user",
    type=int,
    metavar="<user-id>",
    help="Set the assignee of the job by user ID",
)

parser.add_argument(
    "job_id",
    type=int,
    metavar="<job-id>",
    help="ID of job to assign",
)

args = parser.parse_args()

# Load config >>>>
if os.path.exists("config.yml"):
    with open("config.yml", "r") as f:
        config = yaml.safe_load(f)
else:
    with open("../config.yml", "r") as f:
        config = yaml.safe_load(f)


def api_url(path):
    c = config["connection"]
    return f"{c['protocol']}://{c['domain']}:{c['port']}/api/{path}"


auth = rq.auth.HTTPBasicAuth("api_admin", "QfSp936y5sF;CTZ8p;5Wpg2C-6q6;1")
# <<<<<<<<

# Fill payload
payload = dict()

if args.stage:
    assert args.stage in {'annotation', 'validation', 'acceptance'}, \
        f"Provided stage `{args.stage}` is invalid"

    payload["stage"] = args.stage

if args.state:
    assert args.state in {'new', 'in progress', 'completed', 'rejected'}, \
        f"Provided state `{args.state}` is invalid"

    payload["state"] = args.state

if args.user is not None:
    payload["assignee"] = int(args.user)

# Send request
response = rq.patch(api_url(f"jobs/{args.job_id}"), json=payload, auth=auth)

assert response.status_code == 200, f"Error code from api: {response.status_code}"
